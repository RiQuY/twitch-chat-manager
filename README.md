# Twitch Chat Manager

Twitch Chat Manager is a Python utility to manage a Twitch chat using a ChatBot.

## Features

This project is in early stages of development and currently supports the following interactions:
- Obtain messages from the chat of any Twitch channel.
- *(Not fully implemented)* Convert text to voice using Piper-TTS and play or save them to a WAV file.
- *(Not fully implemented)* Convert text to voice using StreamElements-TTS and play or save them to a WAV file.
- *(Not fully implemented)* Convert messages from Twitch chat to audio and play it.

## Prerequisites

This repo was developed using openSUSE Tumbleweed, although it is not necessary to use that GNU/Linux distribution the installation steps would be based in that environment.
- [Python](https://www.python.org/) 3.11.8 *(The exact version is not mandatory)*
- [Git](https://git-scm.com/)
- [Register a Twitch Dev App](https://dev.twitch.tv/docs/authentication/register-app/) *(You will need to use your own CLIENT_ID and CLIENT_SECRET in the next sections)*

Audio players.
- ffmpeg

**NOTE**: When registering a Twitch Dev App remember to set OAuth URL in Twitch App Console to: http://localhost:17563 because that's the URL **twitchAPI** dependencty uses to obtain the User Token.

## Installation

Before installing any dependencies make sure to create a Python Virtual Enviroment and activate it in the root folder of the project using these commands:

```bash
python3 -m  venv env
source env/bin/activate
```

The proceed installing the [pip](https://pip.pypa.io/en/stable/) dependencies.

```bash
pip3 install -r requirements.txt
```

## Configuring

Before start using this utility you need to create a **.env** file in the project's root folder and fill this fields using your own credentials to be able to interact with the Twitch API.

```
CLIENT_ID=<YOUR_CLIENT_ID>
CLIENT_SECRET=<YOUR_CLIENT_SECRET>
CHANNEL=<ANY_CHANNEL_NAME>
```

## Usage

Before using the utility remember to enable the Python Virtual Enviroment.

```bash
source env/bin/activate
```

### ChatBot

If this is your first time executing the utility, a browser page will open asking for permissions to connect to your Twitch Dev App, if the connection was successful you will see the following message in your terminal: `Bot is ready for work, joining channels` and any messages received on the chat will appear on the terminal too.

*(In development)* The ChatBot uses Pipper-TTS and every message received will be played as a sound.

Available parameters:
- `--enable_tts`: *(Optional)* Enables the TTS funcionality of the ChatBot.
- `--model "<MODEL_IDENTIFIER>"`: *(If TTS is enabled)* Define the Piper-TTS model to use, to see the available models check [HuggingFace repo](https://huggingface.co/rhasspy/piper-voices/tree/main)
- `--speaker <SPEAKER_NUMBER>`: *(If TTS is enabled)* *(Optional)* Only for multispeaker models, available speakers for each model can be found in the MODEL_CARD in the [HuggingFace repo](https://huggingface.co/rhasspy/piper-voices/tree/main) (default: 0).

Usage example:

```bash
python3 ./main.py --enable_tts
```

### Piper-TTS reader (Manual)

You can generate audio output or save to a file executing the file `tss_speaker.py` manually without needing the Twitch integration.

Available parameters:
- `--model "<MODEL_IDENTIFIER>"`: Define the Piper-TTS model to use, to see the available models check [HuggingFace repo](https://huggingface.co/rhasspy/piper-voices/tree/main)
- `--text "<TEXT_TO_PROCESS>"`: Plain text to process.
- `--play_audio`: *(Optional)* Play audio directly.
- `--save_audio`: *(Optional)* Save the audio generated to a file, by default it saves the file in the `output` folder with the current timestamp.
- `--speaker "<SPEAKER_NUMBER>"`: *(Optional)* Only for multispeaker models, available speakers for each model can be found in the MODEL_CARD in the [HuggingFace repo](https://huggingface.co/rhasspy/piper-voices/tree/main) (default: 0).

Usage example:

```bash
python3 ./tss_speaker.py --model "es_ES-sharvard-medium" --play-audio --text "Hola mundo."
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

[comment]: <Please make sure to update tests as appropriate.>

## License

Project license: [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)

The models licenses can be found inside each model folder in the [HuggingFace repo](https://huggingface.co/rhasspy/piper-voices/tree/main).
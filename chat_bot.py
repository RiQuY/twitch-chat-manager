import os
import asyncio

from twitchAPI.twitch import Twitch
from twitchAPI.oauth import UserAuthenticator
from twitchAPI.type import AuthScope, ChatEvent
from twitchAPI.chat import Chat, EventData, ChatMessage, ChatSub, ChatCommand


# this will be called when the event READY is triggered, which will be on bot start
async def on_ready(ready_event: EventData):
    print('Bot is ready for work, joining channels')
    # join our target channel, if you want to join multiple, either call join for each individually
    # or even better pass a list of channels as the argument
    await ready_event.chat.join_room(os.getenv('CHANNEL'))
    # you can do other bot initialization things in here


# this will be called whenever a message in a channel was send by either the bot OR another user
async def on_message(msg: ChatMessage):
    print(f'in {msg.room.name}, {msg.user.name} said: {msg.text}')

# this will be called whenever the !redes command is issued
# async def test_command(cmd: ChatCommand):
#     await cmd.reply(f'{cmd.user.name}: https://linksta.cc/@RiQuY')

# this is where we set up the bot
async def run():
    # set up twitch api instance and add user authentication with some scopes
    USER_SCOPE = [AuthScope.CHAT_READ, AuthScope.CHAT_EDIT]
    twitch = await Twitch(os.getenv('CLIENT_ID'), os.getenv('CLIENT_SECRET'))
    # IMPORTANT: Set OAuth URL in Twitch App Console to: http://localhost:17563
    auth = UserAuthenticator(twitch, USER_SCOPE)
    token, refresh_token = await auth.authenticate()
    await twitch.set_user_authentication(token, USER_SCOPE, refresh_token)

    # create chat instance
    chat = await Chat(twitch)

    # register the handlers for the events you want

    # listen to when the bot is done starting up and ready to join channels
    chat.register_event(ChatEvent.READY, on_ready)
    # listen to chat messages
    chat.register_event(ChatEvent.MESSAGE, on_message)

    # you can directly register commands and their handlers, this will register the !redes command
    # chat.register_command('redes', test_command)

    # we are done with our setup, lets start this bot up!
    chat.start()

    # lets run till we press enter in the console
    try:
        input('press ENTER to stop\n')
    finally:
        # now we can close the chat bot and the twitch api client
        chat.stop()
        await twitch.close()


def start_chat_bot():
    # lets run our setup
    asyncio.run(run())

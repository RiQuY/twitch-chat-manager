
from dotenv import load_dotenv

from chat_bot import start_chat_bot

load_dotenv()

if __name__ == "__main__":
    start_chat_bot()


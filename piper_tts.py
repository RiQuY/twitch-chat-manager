# Process flow
# https://stackoverflow.com/a/76233311

import subprocess
from datetime import datetime


def generate_audio(text, model, speaker):
    # Pass text to the echo command
    echo_command = ['echo', text]
    # Execute piper-tts with the model provided and output the result as RAW audio
    piper_command = ['piper', '--model', model, '--speaker', speaker,
                     '--data_dir', 'models', '--download-dir', 'models', '--output-raw']

    # Execute the process flow
    echo_process = subprocess.Popen(echo_command, stdout=subprocess.PIPE)
    piper_process = subprocess.Popen(
        piper_command, stdin=echo_process.stdout, stdout=subprocess.PIPE)

    # Let stream flow between them
    output, _ = piper_process.communicate()

    return output


def play_audio(raw_audio):
    # Play RAW audio with ffplay
    audio_command = ['ffplay', '-f', 's16le',
                     '-ar', '22050', '-nodisp', '-autoexit', '-']

    # Execute the process flow
    audio_process = subprocess.Popen(
        audio_command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    # Let stream flow between them
    output, _ = audio_process.communicate(input=raw_audio)


def save_audio(text, model, speaker):
    now = datetime.now()
    date_time = now.strftime("%m-%d-%Y_%H:%M:%S")
    output_file = f"output/output_{date_time}.wav"

    # Pass text to the echo command
    echo_command = ['echo', text]
    # Execute piper-tts with the model provided and output the result as VAW audio
    piper_command = ['piper', '--model', model, '--speaker', speaker,
                     '--data_dir', 'models', '--download-dir', 'models', '--output_file', output_file]

    # Execute the process flow
    echo_process = subprocess.Popen(echo_command, stdout=subprocess.PIPE)
    piper_process = subprocess.Popen(
        piper_command, stdin=echo_process.stdout, stdout=subprocess.PIPE)

    # Let stream flow between them
    output, _ = piper_process.communicate()


if __name__ == "__main__":
    text = 'Furro el que lo escuche'
    model = 'es_ES-sharvard-medium'  # 2 Speakers
    # Only use this param with multispeaker models
    speaker = '1'

    # save_audio(text, model, speaker)
    raw_audio = generate_audio(text, model, speaker)
    play_audio(raw_audio)

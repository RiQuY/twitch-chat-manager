# Streamelements API usage
# https://github.com/chrisjp/tts/blob/998928b5f564b5799c4a2bb9b18f667750a7b7c2/src/Services/StreamElements.php#L91C21-L91C31

# List of voices
# https://github.com/chrisjp/tts/blob/998928b5f564b5799c4a2bb9b18f667750a7b7c2/assets/js/voices.json#L167

import subprocess
from datetime import datetime

import requests


def generate_audio(voice: str, text: str):
    base_url = 'https://api.streamelements.com/kappa/v2/speech?'
    params = {
        'voice': voice,
        'text': text
    }
    headers = {'Accept': 'audio/mp3'}
    r = requests.get(base_url, params, headers=headers)

    return r.content


def save_audio(mp3_audio):
    now = datetime.now()
    date_time = now.strftime("%m-%d-%Y_%H:%M:%S")
    output_file = f"output/output_{date_time}.wav"

    # Convert MP3 audio to VAW and export it to a file
    conversion_command = ['ffmpeg', '-i', '-', '-f', 'wav', '-y', output_file]

    # Execute the process flow
    conversion_process = subprocess.Popen(
        conversion_command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    # Let stream flow between them
    output, _ = conversion_process.communicate(input=mp3_audio)


def play_audio(mp3_audio):
    # Play RAW audio with ffplay
    audio_command = ['ffplay', '-nodisp', '-autoexit', '-']

    # Execute the process flow
    audio_process = subprocess.Popen(
        audio_command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    # Let stream flow between them
    output, _ = audio_process.communicate(input=mp3_audio)


if __name__ == "__main__":
    tts_voice = {"vid": "es-ES-Standard-A", "name": "Rosalinda",
                 "flag": "ES", "lang": "Spanish", "accent": "Castilian", "gender": "F"}
    voice = 'es-ES-Standard-A'
    text = 'Furro el que lo escuche'

    mp3_audio = generate_audio(voice, text)
    play_audio(mp3_audio)
    # save_audio(mp3_audio)
